# Generated by Django 4.0.3 on 2023-12-21 23:40

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='appointment',
            name='sold',
            field=models.ForeignKey(default=False, on_delete=django.db.models.deletion.CASCADE, related_name='appointments', to='service_rest.automobilevo'),
        ),
        migrations.AlterField(
            model_name='automobilevo',
            name='vin',
            field=models.CharField(max_length=17),
        ),
    ]
