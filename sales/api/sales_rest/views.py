from django.shortcuts import render
from .models import AutomobileVO, Salesperson, Customer, Sale
from common.json import ModelEncoder
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json


# Create your views here.
class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
    ]


class SalespersonListEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "first_name",
        "last_name",
        "employee_id",
    ]


class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = [
        "first_name",
        "last_name",
        "address",
        "phone_number",
    ]


class SaleListEncoder(ModelEncoder):
    model = Sale
    properties = [
        "pk",
        "price",
        "automobile",
        "salesperson",
        "customer",
    ]

    def get_extra_data(self, o):
        return {
            "automobile": {
                "vin": o.automobile.vin,
                "sold": o.automobile.sold,
            },
            "salesperson": {
                "employee_id": o.salesperson.employee_id,
                "first_name": o.salesperson.first_name,
                "last_name": o.salesperson.last_name,
            },
            "customer": {
                "first_name": o.customer.first_name,
                "last_name": o.customer.last_name
            },
        }


class SaleDetailEncoder(ModelEncoder):
    model = Sale
    properties = [
        "price",
        "automobile",
        "salesperson",
        "customer",
    ]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "salesperson": SalespersonListEncoder(),
        "customer": CustomerListEncoder(),
    }


@require_http_methods(["GET", "POST"])
def list_salespeople(request):
    if request.method == "GET":
        try:
            salespeople = Salesperson.objects.all()
            return JsonResponse(
                {"salespeople": salespeople},
                encoder=SalespersonListEncoder,
            )
        except:
            response = JsonResponse(
                {"message": "Could not compile salespeople"}
            )
            response.status_code = 400
            return response
    else:
        try:
            content = json.loads(request.body)
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalespersonListEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the salesperson"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE"])
def delete_sperson(request, pk):
    if request.method == "DELETE":
        try:
            salesperson = Salesperson.objects.get(id=pk)
            salesperson.delete()
            return JsonResponse(
                {"salesperson": salesperson},
                encoder=SalespersonListEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse(
                {"message": "Salesperson does not exist"}
            )
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def list_customers(request):
    if request.method == "GET":
        try:
            customers = Customer.objects.all()
            return JsonResponse(
                {"customers": customers},
                encoder=CustomerListEncoder,
            )
        except:
            response = JsonResponse(
                {"message": "Could not compile customers"}
            )
            response.status_code = 400
            return response
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerListEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the customer"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE"])
def delete_customer(request, pk):
    if request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=pk)
            customer.delete()
            return JsonResponse(
                {"customer": customer},
                encoder=CustomerListEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            response = JsonResponse(
                {"message": "Customer does not exist"}
            )
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def list_sales(request):
    if request.method == "GET":
        try:
            sales = Sale.objects.all()
            return JsonResponse(
                {"sales": sales},
                encoder=SaleListEncoder,
            )
        except:
            response = JsonResponse(
                {"message": "Could not compile sales"}
            )
            response.status_code = 400
            return response
    else:
        try:
            content = json.loads(request.body)
            employee_id = content["salesperson"]
            salesperson = Salesperson.objects.get(employee_id=employee_id)
            content["salesperson"] = salesperson
            phone_number = content["customer"]
            customer = Customer.objects.get(phone_number=phone_number)
            content["customer"] = customer
            automobile = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=automobile)
            content["automobile"] = automobile
            sale = Sale.objects.create(**content)
            return JsonResponse(
                sale,
                encoder=SaleDetailEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the sale"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE"])
def delete_sale(request, pk):
    if request.method == "DELETE":
        try:
            sale = Sale.objects.get(id=pk)
            sale.delete()
            return JsonResponse(
                {"sale": sale},
                encoder=SaleListEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            response = JsonResponse(
                {"message": "Sale does not exist"}
            )
            response.status_code = 404
            return response
