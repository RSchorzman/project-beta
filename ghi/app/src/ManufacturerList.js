import { useEffect, useState } from "react";

function ManufacturerList() {
    const [manufacturers, setManus] = useState([])
    const getManusData = async () => {
        const response = await fetch('http://localhost:8100/api/manufacturers/');
        if (response.ok) {
            const manusData = await response.json();
            setManus(manusData.manufacturers)
        }
    }

    useEffect(() =>{
        getManusData()
    }, [])

    return (
        <div>
            <h1>Manufacturers</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Name</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturers.map(manufacturer => {
                        return (
                        <tr key={manufacturer.id}>
                            <td>{ manufacturer.name }</td>
                        </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default ManufacturerList
