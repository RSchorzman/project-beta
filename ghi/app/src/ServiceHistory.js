import { useEffect, useState } from "react";


function ServiceHistory() {
    const [appointments, setAppointments] = useState([])
    const getAppointmentsData = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/');
        if (response.ok) {
            const appointmentsData = await response.json();
            setAppointments(appointmentsData.appointments)
        }
    }

    const [autos, setAutos] = useState([])
    const getAutosData = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles/');
        if (response.ok) {
            const autosData = await response.json();
            setAutos(autosData.autos)
        }
    }

    function ifVip(appointment) {
        for (let x of autos) {
            if (appointment.vin === x.vin) {
                return "Yes"
            }
        } return "No"
    };

    const [vinChange, setVinChange] = useState("");
    const handleVinChange = (event) => {
        event.preventDefault();
        setVinChange(event.target.value);
    };

    useEffect(() =>{
        getAppointmentsData();
        getAutosData();
    }, [])

    const [vinSearch, setVinSearch] = useState("");
    const handleVinSearch = (event) => {
        event.preventDefault();
        setVinSearch(vinChange);
    };


    return (
        <div>
            <h1>Service History</h1>
            <form onSubmit={handleVinSearch}>
                <div className="form-floating mb-1">
                    <input onChange={handleVinChange} value={vinChange} placeholder="Enter VIN" type="text" name="vin" id="vin" className="form-control"/>
                    <label htmlFor="vin">VIN Search</label>
                    <button type="submit" className="btn btn-primary">Search</button>
                </div>
            </form>

            <table className="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">VIN</th>
                        <th scope="col">VIP</th>
                        <th scope="col">Customer</th>
                        <th scope="col">Date</th>
                        <th scope="col">Time</th>
                        <th scope="col">Technician</th>
                        <th scope="col">Reason</th>
                        <th scope="col">Status</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.filter(appointment => appointment.vin === vinSearch || vinSearch === "").map(appointment => {
                        const date = new Date(appointment.date_time);
                        const dateString = date.toDateString();
                        const timeString = date.toLocaleTimeString();
                        return (
                        <tr key={appointment.id}>
                            <td>{ appointment.vin }</td>
                            <td>{ ifVip(appointment) }</td>
                            <td>{ appointment.customer }</td>
                            <td>{ dateString }</td>
                            <td>{ timeString }</td>
                            <td>{ appointment.technician.first_name }</td>
                            <td>{ appointment.reason }</td>
                            <td>{ appointment.status }</td>
                        </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default ServiceHistory
