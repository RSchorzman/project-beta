import React, {useState} from "react";
import { useNavigate } from "react-router-dom";

function ManufacturerForm() {
    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const navigate = useNavigate();
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.name = name;
        const manusUrl = 'http://localhost:8100/api/manufacturers/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(manusUrl, fetchConfig);
        if (response.ok) {
            setName('');
            navigate('/manufacturers');
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Manufacturer</h1>
                    <form onSubmit={handleSubmit} id="create-manufacturer">
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} value={name} required placeholder="Manufacturer Name" type="text" id="name" name="name" className="form-control" />
                            <label htmlFor="name">Manufacturer Name</label>
                        </div>
                        <button className="btn btn-primary">Add Manufacturer</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default ManufacturerForm
