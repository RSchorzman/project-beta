import { useEffect, useState } from "react";

function SalesHistory() {

    const [salesperson, setSP] = useState('');
    const handleSPChange = (event) => {
        const value = event.target.value;
        setSP(value);
    }

    const [salespeople, setSPs] = useState([]);
    const getSPData = async () => {
        const response = await fetch('http://localhost:8090/api/salespeople/');
        if (response.ok) {
            const spData = await response.json();
            setSPs(spData.salespeople);
        }
    }

    const [sales, setSales] = useState([])
    const getSaleData = async () => {
        const response = await fetch('http://localhost:8090/api/sales/');
        if (response.ok) {
            const saleData = await response.json();
            setSales(saleData.sales)
        }
    }
    const filterSales = sales.filter((sale) => sale.salesperson.employee_id === salesperson)

    useEffect(() =>{
        getSaleData();
        getSPData();
    }, [])

    return (
        <div>
            <h1>Salesperson History</h1>
            <div className="mb-1">
                <select onChange={handleSPChange} value={salesperson} required name="salesperson" id="salesperson" className="form-select">
                    <option value="">Select a Salesperson</option>
                    {salespeople.map(salesperson => {
                        return (
                            <option key={salesperson.employee_id} value={salesperson.employee_id}>
                                {salesperson.first_name} {salesperson.last_name}
                            </option>
                        )
                    })}
                </select>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Salesperson</th>
                        <th scope="col">Customer</th>
                        <th scope="col">VIN</th>
                        <th scope="col">Price</th>
                    </tr>
                </thead>
                <tbody>
                    {filterSales.map(sale => {
                        return (
                        <tr key={sale.pk}>
                            <td>{ sale.salesperson.first_name } { sale.salesperson.last_name }</td>
                            <td>{ sale.customer.first_name } { sale.customer.last_name }</td>
                            <td>{ sale.automobile.vin }</td>
                            <td>${ sale.price }</td>
                        </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default SalesHistory
